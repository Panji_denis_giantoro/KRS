<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tbl_login extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'username' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '15',
                                
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '45',
                        ),
                        'stts' => array(
                                'type' => 'date',
                           
                        ),
                        'alamat' => array(
                                'type' => 'text',
                                
                        ),
                        'no_hp' => array(
                                'type' => 'varchar',
                                 'constraint' => 12,
                                'null' => TRUE,
                        ),
                        'jurusan' => array(
                                'type' => 'enum',
                               'constraint' => array ('S1 - Teknik Informatika', 'D3 - Komputerisasi'),
                                
                        ),
                        'thn_masuk' => array(
                                'type' => 'year',
                                 'constraint' => 4,
                               
                        ),
                        'username' => array(
                                'type' => 'varchar',
                                 'constraint' => '12',
                              
                        ),
                          'password' => array(
                                'type' => 'varchar',
                                 'constraint' => '10',
                              
                        ),


                ));
                $this->dbforge->add_key('npm', TRUE);
                $this->dbforge->create_table('t_mahasiswa');
        }

        public function down()
        {
                $this->dbforge->drop_table('t_mahasiswa');
        }
}